export const environment = {
  production: true,
   elasticsearchBaseUrl: 'http://t3elastic1:10200/foresia',
   useElastic24Syntax: true
  // //  elasticsearchBaseUrl: 'http://localhost:9200/fenix'
};
