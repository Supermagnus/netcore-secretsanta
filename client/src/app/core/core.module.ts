import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from 'selenium-webdriver/http';
import { ElasticFenixService } from './services/elasticFenixService';
import { HttpClientModule } from '@angular/common/http';
import localesv from '@angular/common/locales/sv';
import { registerLocaleData } from '@angular/common';
import {LOCALE_ID} from '@angular/core';

registerLocaleData(localesv);
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: 'IFenixService', useClass: ElasticFenixService
    },
    { provide: LOCALE_ID, useValue: 'sv' },

  ]

})
export class CoreModule { }
