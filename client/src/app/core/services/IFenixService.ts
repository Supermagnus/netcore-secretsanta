import { Observable } from 'rxjs/internal/Observable';
import { FacetValue } from '../domain/facet/facetValue';
import { ComponentTable } from '../domain/fenix/tables/componentTable';
import { Description } from '../domain/fenix/tables/description';
import { DeviceTable } from '../domain/fenix/tables/deviceTable';
import { SystemNumberTable } from '../domain/fenix/tables/systemNumberTable';
import { SearchRequest } from '../domain/searchRequest';
import { Field } from '../domain/field';
import { DateFacetResult } from '../domain/facet/dateFacetResult';
import { DateInterval } from '../domain/facet/dateInterval';
import { FacilityTable } from '../domain/fenix/tables/facilityTable';


export interface IFenixService {
  getFacilityTable(searchRequest: SearchRequest): Observable<FacilityTable>;
  getSystemNumberTable(searchRequest: SearchRequest): Observable<SystemNumberTable>;
  getDeviceTable(searchRequest: SearchRequest): Observable<DeviceTable>;
  getComponentTable(searchRequest: SearchRequest): Observable<ComponentTable>;

  getFacilityDescription(facilityId: string): Observable<Description>;
  getSystemDescription(systemId: string): Observable<Description>;
  getDeviceDescription(deviceId: string): Observable<Description>;
  getComponentDescription(componentId: string): Observable<Description>;



  getFacetValues(fieldName: string, searchRequest: SearchRequest, limit: number): Observable<FacetValue[]>;
  getDateHistogram(fields: Field[], searchRequest: SearchRequest, dateInterval: DateInterval): Observable<DateFacetResult[]>;
}
