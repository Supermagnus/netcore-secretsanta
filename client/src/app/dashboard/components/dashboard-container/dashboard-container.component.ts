import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DateFacet } from 'src/app/core/domain/facet/dateFacet';
import { TermFacet } from 'src/app/core/domain/facet/termFacet';
import { FenixResult } from 'src/app/core/domain/fenix/fenixResult';
import { FieldMappings } from 'src/app/core/domain/fenix/fieldMappings';
import { SearchRequest } from 'src/app/core/domain/searchRequest';

@Component({
  selector: 'app-dashboard-container',
  templateUrl: './dashboard-container.component.html',
  styleUrls: ['./dashboard-container.component.scss']
})
export class DashboardContainerComponent implements OnInit {

  dateFacets = [
    new DateFacet('Registreringsdatum', FieldMappings.dateCreated)
      .addComparisonField(FieldMappings.plannedFrom)
      .addComparisonField(FieldMappings.plannedTo)
  ];
  termFacets = [
    new TermFacet('Block', FieldMappings.facility, 'Välj vilka block som du vill se underhåll för')
      .setColor('#E0BBE4'),
    new TermFacet('System', FieldMappings.systemNumber, 'Välj vilka system som du vill se underhåll för')
      .setColor('#957DAD'),
    new TermFacet('Apparat', FieldMappings.device, 'Välj vilka apparater som du vill se underhåll för')
      .setColor('#D291BC'),
    new TermFacet('Komponent', FieldMappings.component, 'Välj vilka komponenter som du vill se underhåll för')
      .setColor('#FEC8D8'),
      new TermFacet('Arbetstyp', FieldMappings.workType, 'Välj vilka arbetstyper du vill se')
      .setColor('#FECF18')
  ];

  request: SearchRequest;
  refresh: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  fenixResult: FenixResult;
  constructor() {
    this.request = new SearchRequest();
    this.request.query = 'fenix_number:*-0'
  }

  ngOnInit() {
  }

  search(request: SearchRequest): void {
    this.request = request.clone();
    this.refresh.next(true);
  }

  setResult(result: FenixResult): void {
    this.fenixResult = result;
  }



}
