import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { FenixResult } from 'src/app/core/domain/fenix/fenixResult';
import { FieldMappings } from 'src/app/core/domain/fenix/fieldMappings';
import { FacilityStatus } from 'src/app/core/domain/fenix/tables/facilityStatus';
import { FacilityTable } from 'src/app/core/domain/fenix/tables/facilityTable';
import { SystemNumberStatus } from 'src/app/core/domain/fenix/tables/systemNumberStatus';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { SupersokHelper } from 'src/app/core/services/helpers/supersokHelper';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { TableBaseComponent } from '../table-base.component';
@Component({
    selector: 'app-fenix-facility-table',
    templateUrl: './fenix-facility-table.component.html',
    styleUrls: ['./fenix-facility-table.component.scss']
})
export class FenixFacilityTableComponent extends TableBaseComponent<FacilityStatus, FacilityTable> implements OnInit {
    @Input() refresh: BehaviorSubject<boolean>;
    @Output() fenixResult: EventEmitter<FenixResult> = new EventEmitter<FenixResult>();
    @Input() partionSystemTables = false;
    systemSearchRequest: SearchRequest;
    description = '';
    facilityRendered = '';

    @ViewChild(MatPaginator, { static: false })
    set matPaginator(pag: MatPaginator) {
        this.paginator = pag;
        this.dataSource.paginator = this.paginator;
    }
    @ViewChild('facilityTableSort', { static: false }) set matSort(ms: MatSort) {
        this.sort = ms;
        this.dataSource.sort = this.sort;
    }

    constructor(
        @Inject('IFenixService') fenixService: IFenixService) {
        super(fenixService);
    }

    ngOnInit(): void {
        this.refresh.subscribe(res => this.updateTable(this.searchRequest));
    }

    getTable(searchRequest: SearchRequest): Observable<FacilityTable> {
        return this.fenixService.getFacilityTable(searchRequest)
            .pipe(map(res => {
                this.fenixResult.emit(new FenixResult(res.rows.length, res.totalWorkOrders));
                return res;
            }));
    }
    renderSystemTable(row: FacilityStatus): void {
        this.systemSearchRequest = this.searchRequest.clone();
        this.systemSearchRequest.filters.setTermFilter(FieldMappings.facility, [ row.id ]);
        this.description = 'Laddar...';
        this.facilityRendered = row.id;
        this.supersokUrl = SupersokHelper.toSupersokUrl(this.systemSearchRequest);
        this.expandedElement = this.expandedElement === row ? null : row;
        this.fenixService.getFacilityDescription(row.id).subscribe(res => {
            this.description = res.text || '[Ingen beskrivning tillgänglig]';
        });
    }
}
