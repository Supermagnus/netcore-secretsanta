import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { FieldMappings } from 'src/app/core/domain/fenix/fieldMappings';
import { SystemNumberStatus } from 'src/app/core/domain/fenix/tables/systemNumberStatus';
import { SystemNumberTable } from 'src/app/core/domain/fenix/tables/systemNumberTable';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { TableBaseComponent } from '../table-base.component';
import { FenixResult } from 'src/app/core/domain/fenix/fenixResult';
import { SupersokHelper } from 'src/app/core/services/helpers/supersokHelper';
import { TableBase } from 'src/app/core/domain/fenix/tables/tableBase';
import { Pattern } from 'src/app/core/domain/fenix/tables/pattern';
import { PartionedTables } from 'src/app/core/domain/fenix/tables/partionedTables';
import { StatusBase } from 'src/app/core/domain/fenix/tables/statusBase';
@Component({
  selector: 'app-fenix-system-number-table',
  templateUrl: './fenix-system-number-table.component.html',
  styleUrls: ['./fenix-system-number-table.component.scss']
})
export class FenixSystemNumberTableComponent extends TableBaseComponent<SystemNumberStatus, SystemNumberTable> implements OnInit {
  @Input() refresh: BehaviorSubject<boolean>;
  @Input() existingTable: SystemNumberTable;
  @Input() partionedView = false;
  @Output() fenixResult: EventEmitter<FenixResult> = new EventEmitter<FenixResult>();
  deviceSearchRequest: SearchRequest;
  description = '';
  systemNumberRendered = '';

  partions = [
    new Pattern('\\d\\d\\d', '0'),
    new Pattern('\\d\\d1\\d\\d', '100'),
    new Pattern('\\d\\d2\\d\\d', '200'),
    new Pattern('\\d\\d3\\d\\d', '300'),
    new Pattern('\\d\\d4\\d\\d', '400'),
    new Pattern('\\d\\d5\\d\\d', '500'),
    new Pattern('\\d\\d6\\d\\d', '600'),
    new Pattern('\\d\\d7\\d\\d', '700'),
    new Pattern('\\d\\d8\\d\\d', '800'),
    new Pattern('\\d\\d9\\d\\d', '900'),
  ];

  partionedTables: PartionedTables;

  @ViewChild(MatPaginator, { static: false })
  set matPaginator(pag: MatPaginator) {
    this.paginator = pag;
    this.dataSource.paginator = this.paginator;
  }
  @ViewChild('systemTableSort', { static: false }) set matSort(ms: MatSort) {
    this.sort = ms;
    this.dataSource.sort = this.sort;
  }

  constructor(
    @Inject('IFenixService') fenixService: IFenixService) {
    super(fenixService);
  }

  ngOnInit(): void {
    if (this.refresh) {
      this.refresh.subscribe(res => this.updateTable(this.searchRequest));
    } else {
      super.ngOnInit();
    }
  }

  getTable(searchRequest: SearchRequest): Observable<SystemNumberTable> {
    if (this.existingTable) {
      return of(this.existingTable);
    }
    if (this.partionedView) {
      return this.getPartionedTableView(searchRequest);
    }
    return this.fenixService.getSystemNumberTable(searchRequest)
      .pipe(map(res => {
        this.fenixResult.emit(new FenixResult(res.rows.length, res.totalWorkOrders));
        return res;
      }));
  }
  renderDeviceTable(row: SystemNumberStatus): void {
    if (this.partionedView) {
      return this.renderPartionTable(row);
    }
    this.deviceSearchRequest = this.searchRequest.clone();
    this.deviceSearchRequest.filters.setTermFilter(FieldMappings.systemNumber, [row.id]);
    this.description = 'Laddar...';
    this.systemNumberRendered = row.id;
    this.supersokUrl = SupersokHelper.toSupersokUrl(this.deviceSearchRequest);
    this.expandedElement = this.expandedElement === row ? null : row;
    this.fenixService.getSystemDescription(row.id).subscribe(res => {
      this.description = res.text;
    });
  }

  getPartionedTableView(searchRequest: SearchRequest): Observable<TableBase> {
    this.partionedTables = new PartionedTables(this.partions);
    return this.fenixService.getSystemNumberTable(searchRequest)
      .pipe(map(res => {
        this.fenixResult.emit(new FenixResult(res.rows.length, res.totalWorkOrders));
        res.rows.forEach(statusRow => {
          this.partionedTables.addStatus(statusRow);
        });
        this.partionedTables.calculate();
        return this.partionedTables;
      }));
  }

  renderPartionTable(row: StatusBase): void {
    this.expandedElement = this.expandedElement === row ? null : row;
  }

  getTableForPattern(pattern: string): SystemNumberTable {
    if (!this.partionedView) {
      return null;
    }
    return this.partionedTables.getTable(pattern);
  }
}
