import { Inject, Input, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { StatusBase } from 'src/app/core/domain/fenix/tables/statusBase';
import { TableBase } from 'src/app/core/domain/fenix/tables/tableBase';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { TermFilter } from 'src/app/core/domain/filter/termFilter';


export abstract class TableBaseComponent<TRow extends StatusBase, TTable extends TableBase> implements OnInit {
  constructor(
    @Inject('IFenixService') protected fenixService: IFenixService
  ) {  }

  @Input() searchRequest: SearchRequest;
  isLoading = false;
  error: any;
  supersokUrl = '';

  protected  sort: MatSort;
  protected  paginator: MatPaginator;



  expandedElement: TRow | null;
  displayedColumns = ['id', 'numAu', 'numFu', 'total', 'auRatio', 'fuRatio', 'aufuRatio'];
  dataSource = new MatTableDataSource<TRow>();


  ngOnInit(): void {
    this.updateTable(this.searchRequest);
  }

  protected verifyTableBinding(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  /*
   * Given a number from 0 to 1
   */
  getRatioColor(value: number, inverted: boolean = false): string {
    const hue = inverted
      ? ((1 - value) * 120).toString(10)
      : (value * 120).toString(10);
    return ['hsl(', hue, ',100%,50%)'].join('');
  }
  updateTable(request: SearchRequest): void {
    this.isLoading = true;
    this.error = null;
    this.getTable(request).subscribe(
        res => {
            this.error = null;
            this.dataSource = new MatTableDataSource(res.rows as TRow[]);
            this.verifyTableBinding();
            this.isLoading = false;
        },
        err => {
            this.error = err;
            this.isLoading = false;
        });
}
  protected abstract getTable(request: SearchRequest): Observable<TTable>;

}
