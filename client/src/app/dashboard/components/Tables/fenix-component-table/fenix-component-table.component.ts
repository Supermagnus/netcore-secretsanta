import { Component, Inject, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { Observable } from 'rxjs';
import { ComponentStatus } from 'src/app/core/domain/fenix/tables/componentStatus';
import { ComponentTable } from 'src/app/core/domain/fenix/tables/componentTable';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { TableBaseComponent } from '../table-base.component';
import { SupersokHelper } from 'src/app/core/services/helpers/supersokHelper';
import { FieldMappings } from 'src/app/core/domain/fenix/fieldMappings';


@Component({
  selector: 'app-fenix-component-table',
  templateUrl: './fenix-component-table.component.html',
  styleUrls: ['./fenix-component-table.component.scss']
})
export class FenixComponentTableComponent extends TableBaseComponent<ComponentStatus, ComponentTable> {
  description = '';
  componentNumberRendered ='';
  @ViewChild(MatSort, { static: false }) set matSort(ms: MatSort) {
    this.sort = ms;
    this.dataSource.sort = this.sort;
  }


  constructor(
    @Inject('IFenixService') fenixService: IFenixService
  ) {
    super(fenixService);
  }


  getTable(searchRequest: SearchRequest): Observable<ComponentTable> {
    return this.fenixService.getComponentTable(searchRequest);
  }

  getDescription(row: ComponentStatus): void {
    this.expandedElement = this.expandedElement === row ? null : row;
    this.description = 'Laddar...';
    this.fenixService.getComponentDescription(row.id).subscribe(res => {
      this.description = res.text || '[Ingen beskrivning tillgänglig]';
      const request = this.searchRequest.clone();
      request.filters.setTermFilter(FieldMappings.component, [row.id]);
      this.supersokUrl = SupersokHelper.toSupersokUrl(request);
      this.componentNumberRendered = row.id;
    });
  }


}
