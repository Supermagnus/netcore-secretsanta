import { Component, Inject, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { Observable } from 'rxjs';
import { FieldMappings } from 'src/app/core/domain/fenix/fieldMappings';
import { DeviceStatus } from 'src/app/core/domain/fenix/tables/deviceStatus';
import { DeviceTable } from 'src/app/core/domain/fenix/tables/deviceTable';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { TableBaseComponent } from '../table-base.component';
import { SupersokHelper } from 'src/app/core/services/helpers/supersokHelper';


@Component({
  selector: 'app-fenix-device-table',
  templateUrl: './fenix-device-table.component.html',
  styleUrls: ['./fenix-device-table.component.scss']
})
export class FenixDeviceTableComponent extends TableBaseComponent<DeviceStatus, DeviceTable>  {
  description = '';
  componentSearchRequest: SearchRequest;
  deviceNumberRendered = '';

  @ViewChild(MatSort, { static: false }) set matSort(ms: MatSort) {
    this.sort = ms;
    this.dataSource.sort = this.sort;
  }

  constructor(
    @Inject('IFenixService') fenixService: IFenixService
  ) {
    super(fenixService);
  }


  getTable(searchRequest: SearchRequest): Observable<DeviceTable> {
    return this.fenixService.getDeviceTable(searchRequest);

  }

  renderComponentTable(row: DeviceStatus): void {
    this.componentSearchRequest = this.searchRequest.clone();
    this.componentSearchRequest.filters.setTermFilter(FieldMappings.device, [row.id]);
    this.description = 'Laddar...';
    this.deviceNumberRendered = row.id;
    this.supersokUrl = SupersokHelper.toSupersokUrl(this.componentSearchRequest);
    this.expandedElement = this.expandedElement === row ? null : row;
    this.fenixService.getDeviceDescription(row.id).subscribe(res => {
      this.description = res.text;
    });
  }

}
