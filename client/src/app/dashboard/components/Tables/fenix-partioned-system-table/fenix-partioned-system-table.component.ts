import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { FenixResult } from 'src/app/core/domain/fenix/fenixResult';
import { FieldMappings } from 'src/app/core/domain/fenix/fieldMappings';
import { PartionedTables } from 'src/app/core/domain/fenix/tables/partionedTables';
import { StatusBase } from 'src/app/core/domain/fenix/tables/statusBase';
import { SystemNumberStatus } from 'src/app/core/domain/fenix/tables/systemNumberStatus';
import { SystemNumberTable } from 'src/app/core/domain/fenix/tables/systemNumberTable';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { SupersokHelper } from 'src/app/core/services/helpers/supersokHelper';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { TableBaseComponent } from '../table-base.component';
import { Pattern } from 'src/app/core/domain/fenix/tables/pattern';
@Component({
  selector: 'app-fenix-partioned-system-table',
  templateUrl: './fenix-partioned-system-table.component.html',
  styleUrls: ['./fenix-partioned-system-table.component.scss']
})
export class FenixPartionedSystemTableComponent extends TableBaseComponent<SystemNumberStatus, SystemNumberTable> implements OnInit {
  @Input() refresh: BehaviorSubject<boolean>;
  @Output() fenixResult: EventEmitter<FenixResult> = new EventEmitter<FenixResult>();
  description = '';
  partions = [
    new Pattern('\\d\\d\\d', '0'),
    new Pattern('\\d\\d1\\d\\d', '100'),
    new Pattern('\\d\\d2\\d\\d', '200'),
    new Pattern('\\d\\d3\\d\\d', '300'),
    new Pattern('\\d\\d4\\d\\d', '400'),
    new Pattern('\\d\\d5\\d\\d', '500'),
    new Pattern('\\d\\d6\\d\\d', '600'),
    new Pattern('\\d\\d7\\d\\d', '700'),
    new Pattern('\\d\\d8\\d\\d', '800'),
    new Pattern('\\d\\d9\\d\\d', '900'),
  ];

  partionedTables: PartionedTables;
  @ViewChild('systemTableSort', { static: false }) set matSort(ms: MatSort) {
    this.sort = ms;
    this.dataSource.sort = this.sort;
  }

  constructor(
    @Inject('IFenixService') fenixService: IFenixService) {
    super(fenixService);
  }

  ngOnInit(): void {
    if (this.refresh) {
      this.refresh.subscribe(res => this.updateTable(this.searchRequest));
    } else {
      super.ngOnInit();
    }
  }

  getTable(searchRequest: SearchRequest): Observable<PartionedTables> {
    this.partionedTables = new PartionedTables(this.partions);
    return this.fenixService.getSystemNumberTable(searchRequest)
      .pipe(map(res => {
        this.fenixResult.emit(new FenixResult(res.rows.length, res.totalWorkOrders));
        res.rows.forEach(statusRow => {
          this.partionedTables.addStatus(statusRow);
        });
        this.partionedTables.calculate();
        return this.partionedTables;
      }));
  }

  renderPartionTable(row: StatusBase): void {
    this.expandedElement = this.expandedElement === row ? null : row;
  }

  getTableForPattern(pattern: string): SystemNumberTable {
    return this.partionedTables.getTable(pattern);
  }
}
