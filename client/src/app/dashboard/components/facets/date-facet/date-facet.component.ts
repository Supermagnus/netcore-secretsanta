import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Inject, ViewChild } from '@angular/core';
import { DateFacet } from 'src/app/core/domain/facet/dateFacet';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { Label, BaseChartDirective } from 'ng2-charts';
import { ChartDataSets, ChartData, ChartOptions } from 'chart.js';
import { FormControl } from '@angular/forms';
import { Field } from 'src/app/core/domain/field';
import { FacetValue } from 'src/app/core/domain/facet/facetValue';
import { DateInterval } from 'src/app/core/domain/facet/dateInterval';
import { DateHelper } from 'src/app/core/services/helpers/dateHelper';

@Component({
  selector: 'app-date-facet',
  templateUrl: './date-facet.component.html',
  styleUrls: ['./date-facet.component.scss']
})
export class DateFacetComponent implements OnChanges, OnInit {

  availableIntervals = [
    { interval: DateInterval.day, display: 'Dag' },
    { interval: DateInterval.week, display: 'Vecka' },
    { interval: DateInterval.month, display: 'Månad' },
    { interval: DateInterval.quarter, display: 'Kvartal' },
    { interval: DateInterval.year, display: 'År' }
  ]

  @Input() dateFacet: DateFacet;
  @Input() searchRequest: SearchRequest;
  @Output() executeSearch: EventEmitter<SearchRequest> = new EventEmitter<SearchRequest>();

  dateInterval: DateInterval = DateInterval.month;
  datesController = new FormControl();
  comparisonFields: Field[];
  @ViewChild(BaseChartDirective, { static: false }) private chart: BaseChartDirective;
  public lineChartData: ChartDataSets[];
  public lineChartLabels: Array<Label>;

  startDate: Date;
  endDate: Date;
  selectStart = true;

  lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
        }

      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };

  constructor(@Inject('IFenixService') private fenixService: IFenixService) {
    this.datesController.valueChanges.subscribe(res => {
      this.comparisonFields = res;
      this.updateGraph();
    })
  }
  ngOnInit(): void {
    this.lineChartOptions.title = {
      display: true,
      text: this.dateFacet.field.displayName
    };
  }
  ngOnChanges(changes: SimpleChanges): void {
    // tslint:disable-next-line:no-string-literal
    if (changes['searchRequest']) {
      const filter = this.searchRequest.filters.getDateFilter(this.dateFacet.field.fieldName);
      this.startDate = filter ? filter.dateStart : null;
      this.endDate = filter ? filter.dateEnd : null;
      this.updateGraph();
    }
  }

  public chartClicked({ event, active }: { event: MouseEvent, active: any[] }): void {
    const firstElement = active ? active[0] : null;
    if (!firstElement) {
      return;
    }
    const label = firstElement._model.label;
    if (!label) {
      return;
    }
    const date = DateHelper.parseDateLabel(label, this.dateInterval);
    if (this.selectStart) {
      this.startDate = date;
      this.searchRequest.filters.setDateFilter(this.dateFacet.field.fieldName, date, this.endDate);
      this.executeSearch.emit(this.searchRequest);
    } else {
      this.endDate = date;
      this.searchRequest.filters.setDateFilter(this.dateFacet.field.fieldName, this.startDate, date);

      this.executeSearch.emit(this.searchRequest);
    }
    this.selectStart = !this.selectStart;
  }

  updateGraph(): void {
    const fields = [this.dateFacet.field].concat(this.comparisonFields).filter(f => !!f);
    this.fenixService.getDateHistogram(fields, this.searchRequest, this.dateInterval).subscribe(res => {
      this.lineChartData = new Array<ChartDataSets>();
      this.lineChartLabels = new Array<Label>();
      const main = res.find(f => f.field.fieldName === this.dateFacet.field.fieldName);

      this.lineChartData.push(this.mapChartDataSet(main.field, main.facetValues, true));
      res.filter(f => f.field.fieldName != main.field.fieldName)
        .forEach(values => this.lineChartData.push(this.mapChartDataSet(values.field, values.facetValues, false)));
    });
  }

  private mapChartDataSet(field: Field, values: FacetValue[], setLabels: boolean): ChartDataSets {
    const dataArray = new Array<number>();
    values.forEach(val => {
      dataArray.push(val.count);
      if (setLabels) {
        this.lineChartLabels.push(val.value);
      }
    });
    return {
      data: dataArray,
      label: field.displayName
    };
  }

  applyDateFilter(startDate: string, endDate: string): void {
    const start = startDate.trim().length > 0 ? new Date(startDate) : null;
    const end = endDate.trim().length > 0 ? new Date(endDate) : null;
    this.searchRequest.filters.setDateFilter(this.dateFacet.field.fieldName, start, end);
    this.executeSearch.emit(this.searchRequest);
  }


}
