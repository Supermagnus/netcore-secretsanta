import { Field } from 'src/app/core/domain/field';

export class AddFacetSheetConfig{
    constructor(public availableTermFields: Field[], public availableDateFields: Field[]){}
}