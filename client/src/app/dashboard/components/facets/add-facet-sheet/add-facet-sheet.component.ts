import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Inject, ViewChild } from '@angular/core';
import { DateFacet } from 'src/app/core/domain/facet/dateFacet';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { Label, BaseChartDirective } from 'ng2-charts';
import { ChartDataSets, ChartData, ChartOptions } from 'chart.js';
import { FormControl } from '@angular/forms';
import { Field } from 'src/app/core/domain/field';
import { FacetValue } from 'src/app/core/domain/facet/facetValue';
import { DateInterval } from 'src/app/core/domain/facet/dateInterval';
import { DateHelper } from 'src/app/core/services/helpers/dateHelper';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { AddFacetSheetConfig } from './addFacetSheetConfig';

@Component({
  selector: 'app-add-facet-sheet',
  templateUrl: './add-facet-sheet.component.html',
  styleUrls: ['./add-facet-sheet.component.scss']
})
export class AddFacetSheetComponent  {
    constructor(private _bottomSheetRef: MatBottomSheetRef<AddFacetSheetComponent>,
      @Inject(MAT_BOTTOM_SHEET_DATA)  public data: AddFacetSheetConfig) { 
    }

    addFacet(field:Field): void{
      this._bottomSheetRef.dismiss(field);
    }


}
