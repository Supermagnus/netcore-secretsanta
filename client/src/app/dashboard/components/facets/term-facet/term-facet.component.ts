import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, EventEmitter, Inject, Input, OnInit, Output, ElementRef, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { FacetValue } from 'src/app/core/domain/facet/facetValue';
import { TermFacet } from 'src/app/core/domain/facet/termFacet';
import { TermFilter } from 'src/app/core/domain/filter/termFilter';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { IFenixService } from 'src/app/core/services/IFenixService';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { map } from 'rxjs/internal/operators/map';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-term-facet',
  templateUrl: './term-facet.component.html',
  styleUrls: ['./term-facet.component.scss']
})
export class TermFacetComponent implements OnChanges {

  @Input() termFacet: TermFacet;
  @Input() searchRequest: SearchRequest;
  @Output() executeSearch: EventEmitter<SearchRequest> = new EventEmitter<SearchRequest>();

  filter: TermFilter;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredValues: FacetValue[];

  inputControl = new FormControl();
  @ViewChild('valueInput', { static: false }) valueInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  cachedValues: Array<FacetValue>;
  constructor(@Inject('IFenixService') private fenixService: IFenixService) {
    this.inputControl.valueChanges.subscribe(
      input => {
        if (typeof input === 'string') {
          this.fetchFacetValues(input).subscribe(res => this.filteredValues = res);
        }
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.searchRequest) {
      this.cachedValues = null;
      this.filter = this.searchRequest.filters.getTermFilter(this.termFacet.fieldName, true);
    }
  }

  valueSelected(event: MatAutocompleteSelectedEvent): void {
    this.filter.addValue(event.option.value.value);
    this.valueInput.nativeElement.value = '';
    this.inputControl.setValue('');
    this.executeSearch.emit(this.searchRequest);
  }


  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;
      // Add our fruit
      if ((value || '').trim()) {
        this.filter.addValue(value.trim());
      }
      // Reset the input value
      if (input) {
        input.value = '';
      }
      this.inputControl.setValue('');
      this.executeSearch.emit(this.searchRequest);
    }
  }
  onFocus(): void {
    if (typeof this.inputControl.value === 'string' || !this.inputControl.value) {
      this.fetchFacetValues(this.inputControl.value).subscribe(res => this.filteredValues = res);
    }
  }
  remove(value: string): void {
    this.filter.removeValue(value);
    this.executeSearch.emit(this.searchRequest);
  }

  private fetchFacetValues(query: string): Observable<FacetValue[]> {
    if (environment.useElastic24Syntax) {
      return this.fetchFacetValuesV24Elastic(query);
    }

    let filterValue = !query ? '' : query;
    if (!filterValue.endsWith('*')) {
      filterValue += '*';
    }
    const request = this.searchRequest.clone();
    request.filters.removeTermFilter(this.termFacet.fieldName);
    request.addMustQueryPart(this.termFacet.fieldName + ':' + filterValue);
    return this.fenixService.getFacetValues(this.termFacet.fieldName, request, 50).pipe(
      map(res => res.filter(v => !this.filter.contains(v.value)))
    );
  }

  private fetchFacetValuesV24Elastic(query: string): Observable<FacetValue[]> {
    if (!this.cachedValues) {
      const request = this.searchRequest.clone();
      request.filters.removeTermFilter(this.termFacet.fieldName);

      return this.fenixService.getFacetValues(this.termFacet.fieldName, request, 30000).pipe(
        map(
        res => {
          this.cachedValues = res;
          console.log('renewing cache')
          return this.cachedValues.filter(v => (!query || v.value.startsWith(query)) && !this.filter.contains(v.value)).slice(0,50);
        }
      ));
    } else {
      return of(this.cachedValues.filter(v => (!query || v.value.startsWith(query)) && !this.filter.contains(v.value)).slice(0,50));
    }

  }
}
