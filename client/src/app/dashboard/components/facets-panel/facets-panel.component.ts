import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { DateFacet } from 'src/app/core/domain/facet/dateFacet';
import { TermFacet } from 'src/app/core/domain/facet/termFacet';
import { FenixResult } from 'src/app/core/domain/fenix/fenixResult';
import { FieldMappings } from 'src/app/core/domain/fenix/fieldMappings';
import { SearchRequest } from 'src/app/core/domain/searchRequest';
import { AddFacetSheetComponent } from '../facets/add-facet-sheet/add-facet-sheet.component';
import { AddFacetSheetConfig } from '../facets/add-facet-sheet/addFacetSheetConfig';

@Component({
  selector: 'app-facets-panel',
  templateUrl: './facets-panel.component.html',
  styleUrls: ['./facets-panel.component.scss']
})
export class FacetsPanelComponent implements OnInit {
  @Input() searchRequest: SearchRequest;
  @Input() termFacets: TermFacet[];
  @Input() dateFacets: DateFacet[];
  @Input() fenixResult: FenixResult;
  @Output() executeSearch: EventEmitter<SearchRequest> = new EventEmitter<SearchRequest>();

  isExpanded = false;
  constructor(private _bottomSheet: MatBottomSheet) {}

  ngOnInit() {
  }

  internalExecuteSearch(searchRequest: SearchRequest): void {
    this.executeSearch.emit(searchRequest);
  }

  reset(): void {
    this.searchRequest.query = '';
    this.searchRequest.filters.clear();
    this.executeSearch.emit(this.searchRequest);
  }

  openAddFacetSheet(): void {
    this._bottomSheet.open(AddFacetSheetComponent, {
      data: new AddFacetSheetConfig([], [FieldMappings.dateCreated])
    }).afterDismissed().subscribe(res => {
      if (res) {
        this.dateFacets.push(res);
      }
    });
  }
}
