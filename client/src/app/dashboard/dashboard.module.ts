import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardContainerComponent } from './components/dashboard-container/dashboard-container.component';
import { Routes, RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import {MatTableModule} from '@angular/material/table';
import { FenixComponentTableComponent } from './components/Tables/fenix-component-table/fenix-component-table.component';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {MatTabsModule} from '@angular/material/tabs';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSortModule } from '@angular/material/sort';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { FenixSystemNumberTableComponent } from './components/Tables/fenix-system-number-table/fenix-system-number-table.component';
import { FenixDeviceTableComponent } from './components/Tables/fenix-device-table/fenix-device-table.component';
import { FacetsPanelComponent } from './components/facets-panel/facets-panel.component';
import { MatNativeDateModule } from '@angular/material/core';
import { ErrorPanelComponent } from './components/error-panel/error-panel.component';
import { DateFacetComponent } from './components/facets/date-facet/date-facet.component';
import { TermFacetComponent } from './components/facets/term-facet/term-facet.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import { AddFacetSheetComponent } from './components/facets/add-facet-sheet/add-facet-sheet.component';
import { FenixFacilityTableComponent } from './components/Tables/fenix-facility-table/fenix-facility-table.component';
import { FenixPartionedSystemTableComponent } from './components/Tables/fenix-partioned-system-table/fenix-partioned-system-table.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardContainerComponent
  }
];

@NgModule({
  declarations: [
    DashboardContainerComponent,
    FenixFacilityTableComponent,
    FenixPartionedSystemTableComponent,
    FenixSystemNumberTableComponent,
    FenixDeviceTableComponent,
    FenixComponentTableComponent,
    SpinnerComponent,
    FacetsPanelComponent,
    ErrorPanelComponent,
    DateFacetComponent,
    TermFacetComponent,
    AddFacetSheetComponent
  ],
  imports: [
    CommonModule,
    ChartsModule,
    MatTableModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatSortModule,
    FormsModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatBottomSheetModule,
    MatListModule,
    MatTabsModule,
    RouterModule.forChild(routes)

  ],
  exports: [
    DashboardContainerComponent
  ],
  entryComponents: [
    DashboardContainerComponent,
    AddFacetSheetComponent
  ]
})
export class DashboardModule { }
