import { NgModule, Inject } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { HttpModule, Http } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { Ng2BootstrapModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { TopMenuComponent } from './components/topmenu/topmenu.component';
import { AlertComponent } from './components/alert/alert.component';
import { LeftMenuComponent } from './components/leftmenu/leftmenu.component';
import { ProcessInformationComponent } from './components/processInformation/processInformation.component';
import { HomeComponent } from './containers/home/home.component';
import { UsersComponent } from './containers/users/users.component';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { CounterComponent } from './containers/counter/counter.component';
// import { ChatComponent } from './containers/chat/chat.component';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { NgxBootstrapComponent } from './containers/ngx-bootstrap-demo/ngx-bootstrap.component';

import { DomainTableComponent} from './components/tables/domainTable.component';
import { DomainOverview} from './containers/hades/domainOverview/domainOverview.component';


import { ToastModule } from 'ng2-toastr/ng2-toastr'
import { LinkService } from './shared/link.service';
import { UserService } from './shared/user.service';
import { HadesService } from './shared/services/hadesService';
import { AlertService } from './shared/services/alertService';
import { ProcessInformationService} from './shared/services/processInformationService';
// import { ConnectionResolver } from './shared/route.resolver';
import { ORIGIN_URL } from './shared/constants/baseurl.constants';
import { TransferHttpModule } from '../modules/transfer-http/transfer-http.module';


@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
          TopMenuComponent,
          LeftMenuComponent,
        CounterComponent,
        UsersComponent,
        UserDetailComponent,
        HomeComponent,
        // ChatComponent,
        NotFoundComponent,
        NgxBootstrapComponent,
        DomainTableComponent,
        AlertComponent,
        DomainOverview,
        ProcessInformationComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        Ng2BootstrapModule.forRoot(), // You could also split this up if you don't want the Entire Module imported
        TransferHttpModule, // Our Http TransferData method

        ToastModule.forRoot(),

        // App Routing
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            },
            {
                path: 'home', component: HomeComponent,

                // *** SEO Magic ***
                // We're using "data" in our Routes to pass in our <title> <meta> <link> tag information
                // Note: This is only happening for ROOT level Routes, you'd have to add some additional logic if you wanted this for Child level routing
                // When you change Routes it will automatically append these to your document for you on the Server-side
                //  - check out app.component.ts to see how it's doing this
                data: {
                    title: 'Homepage',
                    meta: [{ name: 'description', content: 'This is an example Description Meta tag!' }],
                    links: [
                        { rel: 'canonical', href: 'http://blogs.example.com/blah/nice' },
                        { rel: 'alternate', hreflang: 'es', href: 'http://es.example.com/' }
                    ]
                }
            },
            {
                path: 'counter', component: CounterComponent,
                data: {
                    title: 'Counter',
                    meta: [{ name: 'description', content: 'This is an Counter page Description!' }],
                    links: [
                        { rel: 'canonical', href: 'http://blogs.example.com/counter/something' },
                        { rel: 'alternate', hreflang: 'es', href: 'http://es.example.com/counter' }
                    ]
                }
            },
            {
                path: 'users', component: UsersComponent,
                data: {
                    title: 'Users REST example',
                    meta: [{ name: 'description', content: 'This is User REST API example page Description!' }],
                    links: [
                        { rel: 'canonical', href: 'http://blogs.example.com/chat/something' },
                        { rel: 'alternate', hreflang: 'es', href: 'http://es.example.com/users' }
                    ]
                }
            },
            {
                path: 'ngx-bootstrap', component: NgxBootstrapComponent,
                data: {
                    title: 'Ngx-bootstrap demo!!',
                    meta: [{ name: 'description', content: 'This is an Demo Bootstrap page Description!' }],
                    links: [
                        { rel: 'canonical', href: 'http://blogs.example.com/bootstrap/something' },
                        { rel: 'alternate', hreflang: 'es', href: 'http://es.example.com/bootstrap-demo' }
                    ]
                }
            },

            { path: 'lazy', loadChildren: './containers/lazy/lazy.module#LazyModule'},

            //Hades
            {
                path: 'hades/DomainOverview', component: DomainOverview,
            },



            {
                path: '**', component: NotFoundComponent,
                data: {
                    title: '404 - Not found',
                    meta: [{ name: 'description', content: '404 - Error' }],
                    links: [
                        { rel: 'canonical', href: 'http://blogs.example.com/bootstrap/something' },
                        { rel: 'alternate', hreflang: 'es', href: 'http://es.example.com/bootstrap-demo' }
                    ]
                }
            }



        

        ], {
          // Router options
          useHash: false,
          preloadingStrategy: PreloadAllModules,
          initialNavigation: 'enabled'
        })
    ],
    providers: [
        LinkService,
        UserService,
        HadesService,
        AlertService,
        ProcessInformationService
        // ConnectionResolver
    ]
})
export class AppModuleShared {
}
