﻿export interface IProcessInformation {
    id: string;
    isRunning: boolean;
    serverName: string;
    processType: string;
    info: string[];
}