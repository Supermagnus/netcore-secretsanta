﻿export class MenuItem {
    
    title: string;
    icon: string;
    route: string;
    items: MenuItem[];
    isCollapsed: boolean;

    constructor(title: string, icon: string, route: string, items: MenuItem[]) {
        this.title = title;
        this.icon = icon;
        this.route = route;
        this.items = items;
        this.isCollapsed = true;
    }

    hasChildren(): boolean {
        return this.items.length > 0;
    }

    toggle() {
        this.isCollapsed = !this.isCollapsed;
    }
}