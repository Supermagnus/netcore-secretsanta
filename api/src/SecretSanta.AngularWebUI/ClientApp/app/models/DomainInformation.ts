﻿export class DomainInformation {
    name: string;
    processingStep: number;
    dispatchingEnabled: boolean;
    indexStatusVerificationEnabled: boolean;

    initialCommands: number;
    processedCommands: number;
    ignoredCommands: number;
    dispatchedCommands: number;
    indexVerifiedCommands: number;
    errouneousCommand: number;
    checkedOutCommands: number;
}