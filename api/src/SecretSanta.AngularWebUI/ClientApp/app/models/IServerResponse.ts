﻿export interface IServerResponse {
  message: string;
  alert: string; //success,info, warning, error
}