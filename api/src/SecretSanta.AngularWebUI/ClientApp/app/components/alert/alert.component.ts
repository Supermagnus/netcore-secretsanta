import { Component, ViewContainerRef } from '@angular/core';
import { IServerResponse } from '../../models/IServerResponse';
import { ToastsManager, Toast } from 'ng2-toastr/ng2-toastr';

import {AlertService} from '../../shared/services/alertService'
@Component({
    selector: 'alert-toast',
    templateUrl: './alert.component.html'
})

export class AlertComponent {
    constructor(public toastr: ToastsManager, vcr: ViewContainerRef, alertService: AlertService) {
        alertService.alert = this;
        this.toastr.setRootViewContainerRef(vcr);
    }

    showLoader() : Promise<Toast>{
        return this.toastr.info("loading..");
    }

    show(response: IServerResponse) {
        if (response.alert === 'success') {
            this.showSuccess(response.message);
        }
        else if (response.alert === 'warning') {
            this.showWarning(response.message);
        } else if(response.alert=== 'error') {
            this.showError(response.message);
        }
        else{
            this.showInfo(response.message);
        }
    }



    showResultInLoader(loadingToast:Promise<Toast> , response: IServerResponse) {
        loadingToast.then(t => {
                t.message = response.message;
                t.type = response.alert;
            }
        );
    }

    showSuccess(msg :string) {
        this.toastr.success(msg);

        
    }
   
    showError(msg:string) {
        this.toastr.error(msg);
    }

    showWarning(msg:string) {
        this.toastr.warning(msg);
    }

    showInfo(msg:string) {
        this.toastr.info(msg);
    }
}
