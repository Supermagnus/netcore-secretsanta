import { Component, OnInit } from '@angular/core';
import { IProcessInformation } from '../../models/IProcessInformation';
import { ProcessInformationService } from '../../shared/services/processInformationService';

@Component({
    selector: 'process-information',
    templateUrl: './processInformation.component.html',
    styleUrls: ['./processInformation.component.css']
})

export class ProcessInformationComponent implements OnInit{
    items: IProcessInformation[];

    constructor(private processInformationService: ProcessInformationService) { }


    ngOnInit() {
        this.refresh();
    }

    refresh() {
        this.processInformationService.getSystemInformation().subscribe(result => {
            this.items = result as IProcessInformation[];

        });
    }

    panelClass(item: IProcessInformation): string {
        if (item.isRunning)
            return "panel-success";
        return "panel-danger";
    }
}
