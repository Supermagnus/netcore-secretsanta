import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/MenuItem';
@Component({
    selector: 'left-menu',
    templateUrl: './leftmenu.component.html',
    styleUrls: ['./leftmenu.component.css']
})

export class LeftMenuComponent implements OnInit{
    items: MenuItem[];
    collapsed: false;

    ngOnInit() {
        this.items = [
            new MenuItem("Overview", "glyphicon glyphicon-home white", "",
                new Array<MenuItem>(0)),
            new MenuItem("Styx", "glyphicon glyphicon-search white", "",
                new Array<MenuItem>(0)),
            new MenuItem("Hades", "glyphicon glyphicon-filter white", "",
                new Array<MenuItem>(
                    new MenuItem("test", "glyphicon glyphicon-search white", "hades/DomainOverview", new Array<MenuItem>()),
                    new MenuItem("test2", "glyphicon glyphicon-search white", "hades/DomainOverview2", new Array<MenuItem>())
                )),
            new MenuItem("Charon", "glyphicon glyphicon-import white", "",
                new Array<MenuItem>(0))
        ];
    }
}
