﻿import { Component, OnInit } from '@angular/core';
import { DomainInformation } from '../../models/DomainInformation';
import { HadesService } from '../../shared/services/hadesService';
import { Observable } from 'rxjs/Observable';
@Component({
    selector: 'domain-table',
    templateUrl: './domainTable.component.html',
    styleUrls: ['./domainTable.component.css']
})


export class DomainTableComponent implements OnInit {
    items: DomainInformation[];

    constructor(private hadesService: HadesService) { }

    ngOnInit() {
        this.items = [];
        this.refresh();
    }

    changeDispatchStep(item: DomainInformation, enabled: boolean) {
        this.hadesService.changeDomainDispatchStep(item.name, enabled).subscribe(result => {
            item.dispatchingEnabled = enabled;
        }
        );
    }

    changeVerificationStep(item: DomainInformation, enabled: boolean) {
        this.hadesService.changeDomainVerificationStep(item.name, enabled).subscribe(result => {
            item.indexStatusVerificationEnabled = enabled;

        }
        );
    }
    changeProcessStep(item: DomainInformation, newStep: number) {
        this.hadesService.changeDomainProcessingStep(item.name, newStep).subscribe(result => {
            item.processingStep = newStep;

        }
        );
    }


    resetToInitial(item: DomainInformation) {
        this.hadesService.resetToInitial(item.name).subscribe(result => {
                this.refresh();
        }
        );
    }

    resetToProcessed(item: DomainInformation) {
        this.hadesService.resetToProcessed(item.name).subscribe(result => {
            this.refresh();

        }
        );
    }

    deleteAll(item: DomainInformation) {
        this.hadesService.deleteDomain(item.name).subscribe(result => {
                this.refresh();

        }
        );
    }







    totalCommands(info: DomainInformation): number {
        return info.initialCommands | 0 +
            info.processedCommands | 0 +
            info.dispatchedCommands | 0 +
            info.checkedOutCommands | 0 +
            info.errouneousCommand | 0 +
            info.ignoredCommands | 0;
    }



    update(oldValue: DomainInformation, newValue: DomainInformation) {
        oldValue.initialCommands = newValue.initialCommands | 0;
        oldValue.processedCommands = newValue.processedCommands | 0;
        oldValue.dispatchedCommands = newValue.dispatchedCommands | 0;
        oldValue.checkedOutCommands = newValue.checkedOutCommands | 0;
        oldValue.errouneousCommand = newValue.errouneousCommand | 0;
        oldValue.ignoredCommands = newValue.ignoredCommands | 0;
        console.log('init:' + oldValue.initialCommands + '   proc:' + oldValue.processedCommands);
    }
    refresh() {
        this.hadesService.getDomainInformation().subscribe(result => {
            this.items = result as DomainInformation[];
            
        });
    }
}
