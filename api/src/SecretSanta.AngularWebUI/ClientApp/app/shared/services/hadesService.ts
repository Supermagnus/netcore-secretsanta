﻿import { Injectable, Inject } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { ORIGIN_URL } from '.././constants/baseurl.constants';
import { DomainInformation } from '../../models/DomainInformation';
import { TransferHttp } from '../../../modules/transfer-http/transfer-http';
import { Observable } from 'rxjs/Observable';
import { IServerResponse } from '../../models/IServerResponse';
import {AlertService } from './alertService';
//import { AlertComponent } from '../../components/alert/alert.component';

@Injectable()
export class HadesService {
    alertService : AlertService;

    constructor(
        private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
        private http: Http, // Use for everything else
        @Inject(ORIGIN_URL) private baseUrl: string,
        alertService : AlertService
    ) {
        this.alertService = alertService;
    }

    getDomainInformation(): Observable<DomainInformation[]> {
        return this.transferHttp.get(`${this.baseUrl}/api/hades/domainCommandInformation`);
    }

    changeDomainProcessingStep(name: string, option: number): Observable<string> {
        var loader = this.alertService.alert.showLoader();
        var response = this.transferHttp.put(`${this.baseUrl}/api/hades/ChangeDomainProcessStep?domainName=` + name + '&option=' + option, '');
        response.subscribe(data => {
            this.alertService.alert.showResultInLoader(loader, data);
            //this.alertService.alert.show(data);
        });
        return response;
    }
    changeDomainDispatchStep(name: string, enabled: boolean): Observable<IServerResponse> {
        var response = this.transferHttp.put(`${this.baseUrl}/api/hades/ChangeDomainDispatchStep?domainName=` + name + '&enabled=' + enabled, '');
        response.subscribe(data => {
            this.alertService.alert.show(data);
        });
        return response;
    }
    changeDomainVerificationStep(name: string, enabled: boolean): Observable<string> {
        var response = this.transferHttp.put(`${this.baseUrl}/api/hades/ChangeDomainVerificationStep?domainName=` + name + '&enabled=' + enabled, '');
        response.subscribe(data => {
            this.alertService.alert.show(data);
        });
        return response;
    }

  resetToInitial(name: string): Observable<string> {
    var response = this.transferHttp.put(`${this.baseUrl}/api/hades/resetToInitial?domainName=` + name, '');
    response.subscribe(data => {
      this.alertService.alert.show(data);
    });
    return response;
  }

  resetToProcessed(name: string): Observable<string> {
    var response = this.transferHttp.put(`${this.baseUrl}/api/hades/resetToProcessed?domainName=` + name, '');
    response.subscribe(data => {
      this.alertService.alert.show(data);
    });
    return response;
  }

  deleteDomain(name: string): Observable<string> {
    var response = this.transferHttp.delete(`${this.baseUrl}/api/hades/deleteDomain?domainName=` + name, '');
    response.subscribe(data => {
      this.alertService.alert.show(data);
    });
    return response;
  }


}
