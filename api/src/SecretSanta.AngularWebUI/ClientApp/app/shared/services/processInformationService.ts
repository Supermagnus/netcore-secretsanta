﻿import { Injectable, Inject } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { ORIGIN_URL } from '.././constants/baseurl.constants';
import { TransferHttp } from '../../../modules/transfer-http/transfer-http';
import { Observable } from 'rxjs/Observable';
import { IProcessInformation } from '../../models/IProcessInformation';
import {AlertService } from './alertService';
//import { AlertComponent } from '../../components/alert/alert.component';

@Injectable()
export class ProcessInformationService {
    alertService : AlertService;

    constructor(
        private transferHttp: TransferHttp, // Use only for GETS that you want re-used between Server render -> Client render
        private http: Http, // Use for everything else
        @Inject(ORIGIN_URL) private baseUrl: string,
        alertService : AlertService
    ) {
        this.alertService = alertService;
    }

    getSystemInformation(): Observable<IProcessInformation[]> {
        var hadesStatus = this.transferHttp.get(`${this.baseUrl}/api/hades/systemStatus`);

        return hadesStatus;
    }


}
