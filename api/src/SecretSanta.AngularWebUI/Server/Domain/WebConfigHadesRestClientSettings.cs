using Findwise.Mythology.Common.Core.Settings;
using Findwise.Mythology.Hades.RestClient.Internal;

namespace AspCoreServer
{
    public class WebConfigHadesRestClientSettings : ConfigSettingsBase, IHadesRestClientSettings
    {
        public string HadesClientApiUrl { get; set; }
        public int HadesTimeoutMS { get; set; }
        public bool UseDefaultProxy { get; set; }
        public string HadesClientApiKey { get; set; }
    }
}