namespace Asp2017.Server.Models
{
    public class ServerResponse
    {
        public ServerResponse(string message, Level level)
        {
            Message = message;
            if (level == Level.Success)
                Alert = "success";
            else if (level == Level.Warning)
                Alert = "warning";
            else if (level == Level.Error)
                Alert ="error";
            else
                Alert = "info";
        }
        public string Message { get; set; }
        public string Alert { get; set; }
    }
}