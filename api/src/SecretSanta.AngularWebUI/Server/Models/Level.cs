namespace Asp2017.Server.Models
{
    public enum Level
    {
        Success,
        Info,
        Warning,
        Error
    }
}