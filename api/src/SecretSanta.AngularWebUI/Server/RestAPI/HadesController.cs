﻿using System.Linq;
using Asp2017.Server.Models;
using Findwise.Mythology.Hades.Dto.Dto.Commands;
using Findwise.Mythology.Hades.RestClient;
using Microsoft.AspNetCore.Mvc;

namespace Zeus.WebUI.Server.RestAPI
{
    [Route("api/[controller]")]
    public class HadesController : Controller
    {
        private readonly AdminRestClient _client;

        public HadesController(AdminRestClient client)
        {
            _client = client;
        }


        [HttpGet("domainCommandInformation")]
        public IActionResult DomainCommandInformation([FromQuery] string[] domainNames)
        {
            var domainInformation = _client.Domain.GetDomainCommandInformation(domainNames);
            return Json(domainInformation);
        }

        [HttpPut("ChangeDomainProcessStep")]
        public IActionResult ChangeDomainProcessStep([FromQuery] string domainName, [FromQuery] int option)
        {
            var current = _client.Domain.GetDomainInformation(domainName).First();
            current.ProcessingStep = (ProcessingOptionDto) option;
            _client.Domain.ChangeDomainState(current);
            return Ok(new ServerResponse(
                $"Changed {domainName} processing step to {current.ProcessingStep}",
                Level.Success));
        }
        [HttpPut("ChangeDomainDispatchStep")]
        public IActionResult ChangeDomainDispatchStep([FromQuery] string domainName, [FromQuery] bool enabled)
        {
            var current = _client.Domain.GetDomainInformation(domainName).First();
            current.DispatchingEnabled = enabled;
            _client.Domain.ChangeDomainState(current);
            current = _client.Domain.GetDomainInformation(domainName).First();

            return Ok(new ServerResponse($"Changed {domainName} dispatch step to {current.DispatchingEnabled}", Level.Success));
        }
        [HttpPut("ChangeDomainVerificationStep")]
        public IActionResult ChangeDomainVerificationStep([FromQuery] string domainName, [FromQuery]  bool enabled)
        {
            var current = _client.Domain.GetDomainInformation(domainName).First();
            current.IndexStatusVerificationEnabled= enabled;
            _client.Domain.ChangeDomainState(current);
            return Ok(
                new ServerResponse($"Changed {domainName} verification step to {current.DispatchingEnabled}",
                Level.Success));
        }

        [HttpPut("resetToInitial")]
        public IActionResult ResetToInitial([FromQuery] string domainName)
        {
            _client.Domain.ResetToInitialStep(domainName);
            return Ok(new ServerResponse($"All commands in {domainName} has been set to Initial", Level.Success));
        }


        [HttpPut("resetToProcessed")]
        public IActionResult ResetToProcessed([FromQuery] string domainName)
        {
            _client.Domain.ResetToProcessedStep(domainName);
            return Ok(new ServerResponse($"All commands in {domainName} has been set to Processed", Level.Success));
        }


        [HttpDelete("deleteDomain")]
        public IActionResult DeleteDomain([FromQuery] string domainName)
        {
            _client.Domain.DeleteAllCommands(domainName);
            return Ok(new ServerResponse($"All commands in {domainName} has been Deleted!", Level.Success));
        }

        [HttpGet("systemstatus")]
        public IActionResult GetSystemStatus()
        {
            var statuses= _client.Status.GetSystemStatus(string.Empty);
            return Json(statuses);
        }
    }
}
