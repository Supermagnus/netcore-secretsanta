﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreServer;
using Findwise.Mythology.Hades.RestClient;
using Findwise.Mythology.Hades.RestClient.Internal;
using StructureMap;

namespace Zeus.WebUI.Server.Infrastructure
{
    public class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            ForSingletonOf<IHadesRestClientSettings>().Use<WebConfigHadesRestClientSettings>();
            ForSingletonOf<AdminRestClient>().Use<AdminRestClient>();
        }
    }
}
