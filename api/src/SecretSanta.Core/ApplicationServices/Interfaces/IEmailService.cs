﻿using System.Collections.Generic;
using SecretSanta.Core.Domain;

namespace SecretSanta.Core.ApplicationServices.Interfaces
{
    public interface IEmailService
    {
        void SendLoginEmails(Group group, IList<User> users, string gameUrl);
    }
}