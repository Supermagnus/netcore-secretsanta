﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using Serilog;

namespace SecretSanta.Core.ApplicationServices.Implementations
{
    public class SmtpEmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;
        private readonly string _mailTemplate;

        public SmtpEmailService(EmailSettings emailSettings)
        {
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;

            _emailSettings = emailSettings;
            var assembly = this.GetType().Assembly;
            Stream resource =
                assembly.GetManifestResourceStream(
                    "SecretSanta.Core.ApplicationServices.Implementations.mailTemplate.html");

            StreamReader sr = new StreamReader(resource, Encoding.UTF8);
            _mailTemplate = sr.ReadToEnd();
        }

        public void SendLoginEmails(Group group, IList<User> users, string gameUrl)
        {
            var client = new SmtpClient(_emailSettings.SmtpServer, _emailSettings.SmtpServerPort)
            {
                Credentials = new NetworkCredential(_emailSettings.SendingEmail, _emailSettings.EmailPassword),
                EnableSsl = true
            };


            foreach (var user in users)
            {
                var header = "Välkommen till Hemliga tomten!";

                var body = _mailTemplate
                    .Replace("{userName}", user.UserName)
                    .Replace("{password}", user.Password)
                    .Replace("{gameUrl}", gameUrl)
                    .Replace("{groupName}", group.Name);

//                var body = "Du, just Du, har blivit utvald att få vara med i " + group.Name + " Hemliga Tomten.\r\n" +
//                           "För att börja så öppnar du upp en webbläsare och går till " + gameUrl + ".\r\n" +
//                           "\r\n" +
//                           "Användarnamn: " + user.UserName + "\r\n" +
//                           "Lösenord: " + user.Password + "\r\n" +
//                           "\r\n" +
//                           "Leken går ut på att du ska köpa en present till en slumpvis utvald person. Personen vet inte vem det är som är dess 'Hemliga tomte', bara du vet det!\r\n" +
//                           "\r\n" +
//                           "Har du några frågor kan du alltid kontaka mig. Jag finns här för Dig!" +
//                           "\r\n" +
//                           "\r\n" +
//                           "Julvänlig hälsning,\r\n" +
//                           "Din Magnus";

//                var santa = @" 
//     ____
//   {} _  \
//      |__ \
//     /_____\
//     \o o)\)_______
//     (<  ) /#######\
//   __{'~` }#########|
//  /  {   _}_/########|
// /   {  / _|#/ )####|
                ///   \_~/ /_ \  |####|
//\______\/  \ | |####|
// \__________\|/#####|
//  |__[X]_____/ \###/
//  /___________\
//   |    |/    |
//   |___/ |___/
//  _|   /_|   /
// (___,_(___,_)";

//                body += "\r\n\r\n" + santa;


                try
                {
                    var message = new MailMessage(_emailSettings.SendingEmail, user.Email, header, body)
                    {
                        IsBodyHtml = true
                    };
                    client.Send(message);
                    Log.Information("Sent email to " + user.Email);
                }
                catch (Exception e)
                {
                    Log.Error(e, "Error when sending mail to " + user.Email + ": " + e.Message);
                }
            }
        }
    }
}