﻿using SecretSanta.Core.ApplicationServices.Implementations;
using SecretSanta.Core.ApplicationServices.Interfaces;
using StructureMap;

namespace SecretSanta.Core.Infrastructure
{
    public class CoreRegistry : Registry
    {

        public CoreRegistry()
        {
            ForSingletonOf<EmailSettings>().Use<EmailSettings>();
            ForSingletonOf<IEmailService>().Use<SmtpEmailService>();
        }
    }
}
