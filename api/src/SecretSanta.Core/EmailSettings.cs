﻿using System.Configuration;
using SecretSanta.Core.Settings;

namespace SecretSanta.Core
{
    public class EmailSettings : ConfigSettingsBase
    {
        public string SmtpServer { get; set; }
        public int SmtpServerPort { get; set; }
        public string SendingEmail { get; set;   }
        public string EmailPassword { get; set; }
    }
}
