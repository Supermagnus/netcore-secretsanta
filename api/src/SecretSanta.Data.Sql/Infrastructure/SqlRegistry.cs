using System.Data.SQLite;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Data.Sql.Databases;
using SecretSanta.Data.Sql.Settings;
using StructureMap;

namespace SecretSanta.Data.Sql.Infrastructure
{
    public class SqlRegistry: Registry
    {

        public SqlRegistry()
        {
            ForSingletonOf<SqlSettings>().Use<SqlSettings>();
            ForSingletonOf<IGroupService>().Use<SqlGroupService>();
            ForSingletonOf<IUserService>().Use<SqlUserService>();
            ForSingletonOf<SecretSantaDatabase>().Use<SecretSantaDatabase>();
        }




    }






}