﻿using System.Collections.Generic;

namespace SecretSanta.Data.Sql.Extensions
{
    public static class EnumerableExtensions
    {
        public static string ToSqlInConstraint(this IEnumerable<int> list)
        {
            return string.Format("({0})", string.Join(",", list));
        }
        public static string ToSqlInConstraint(this IEnumerable<string> list)
        {
            return string.Format("('{0}')", string.Join("','", list));
        }
    }
}
