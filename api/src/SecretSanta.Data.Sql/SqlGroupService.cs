using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Dapper.Contrib.Extensions;
using SecretSanta.Core.ApplicationServices.Implementations;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases;
using SecretSanta.Data.Sql.Databases.SecretSanta.Mappers;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql
{
    public class SqlGroupService : GroupServiceBase, IGroupService
    {
        private readonly SecretSantaDatabase _santaDatabase;


        public SqlGroupService(SecretSantaDatabase santaDatabase)
        {
            _santaDatabase = santaDatabase;

            using (var conn = _santaDatabase.GetOpenConnection())
            {
                _santaDatabase.Setup(conn);
            }
        }

        public int SaveGroup(Group group)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                var groupModel = GroupMapper.Map(group);

                int id = group.Id;
                if(!GroupExists(id, connection,transaction)){
                    id = (int) connection.Insert(groupModel, transaction);
                    group.Id = id;
                    SaveGroupUserRelation(group,connection,transaction);
                }
                else
                {
                    connection.Update(groupModel, transaction);
                    var existingUserIdsSql = string.Format("SELECT * FROM {0} WHERE GroupId = @groupId",
                        GroupUserTable.TableName);
                    var existingGroupUsers = connection.Query<GroupUserModel>(existingUserIdsSql,
                        new {groupId = group.Id}, transaction);
                    SaveGroupUserRelation(group,connection,transaction,existingGroupUsers.ToList());
                }
                transaction.Commit();
                group.Id = id;
                return id;
            }
        }

        private void SaveGroupUserRelation(Group group, IDbConnection connection, IDbTransaction transaction, IList<GroupUserModel> existingGroupUsers = null   )
        {
            if(existingGroupUsers == null)
                existingGroupUsers = new List<GroupUserModel>();

            var usersToInsert = group.UserIds
                    .Where(id => !existingGroupUsers.Any(existing => existing.UserId == id && !existing.IsAdmin))
                    .Select(id => new GroupUserModel() {GroupId = group.Id, IsAdmin = false, UserId = id})
                    .ToList();

            var adminUsersToInsert = group.AdminUserIds
                    .Where(id => !existingGroupUsers.Any(existing => existing.UserId == id && existing.IsAdmin))
                    .Select(id => new GroupUserModel() { GroupId = group.Id, IsAdmin = true, UserId = id })
                    .ToList();
            
            foreach (var user in usersToInsert)
                connection.Insert(user, transaction);
            foreach (var admin in adminUsersToInsert)
                connection.Insert(admin, transaction);
        }

        public Group GetGroup(string groupName)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var id =
                    connection.Query<int>(
                        string.Format("SELECT Id FROM {0} WHERE Name = @name", GroupTable.TableName),
                        new {name = groupName}).FirstOrDefault();
                return GetGroup(id);
            }
        }

        public Group GetGroup(int groupId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var groupSql = string.Format("SELECT * FROM {0} WHERE Id = @groupId", GroupTable.TableName);
                var retrievedGroupModel = connection.Query<GroupModel>(groupSql, new {groupId= groupId}).FirstOrDefault();
                if (retrievedGroupModel == null)
                    return null;

                var userIdsSql = string.Format("SELECT * FROM {0} WHERE GroupId = @groupId",
                    GroupUserTable.TableName);
                var retrievedUserIds = connection.Query<GroupUserModel>(userIdsSql, new {groupId = groupId}).ToList();
                var group = GroupMapper.MapBack(retrievedGroupModel, retrievedUserIds);
                return group;
            }
        }

        public IList<Group> GetAllGroups()
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var ids =
                    connection.Query<int>(
                        string.Format("SELECT Id FROM {0}", GroupTable.TableName));
                var groups = ids.Select(GetGroup);
                return groups.ToList();
            }
        }

        public IList<Group> GetGroupsForUser(string userName)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var groupIds =
                    connection.Query<int>(
                        string.Format("SELECT GroupId FROM {0} gu INNER JOIN {1} u ON u.Id = gu.UserId WHERE u.UserName= @userName"
                        , GroupUserTable.TableName, UserTable.TableName),
                        new { userName });
                return groupIds.Select(GetGroup).ToList();
            }
        }

        public IList<Group> GetGroupsForUser(int userId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var groupIds =
                    connection.Query<int>(
                        string.Format("SELECT GroupId FROM {0} WHERE UserId = @userId", GroupUserTable.TableName),
                        new {userId = userId});
                return groupIds.Select(GetGroup).ToList();
            }
        }

        public void DeleteGroup(string groupName)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteGroup(int groupId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            using (var transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                if (!GroupExists(groupId, connection,transaction)) 
                    return;

                connection.Execute("DELETE FROM " + GroupUserTable.TableName + " WHERE GroupId = @id",
                    new { id = groupId }, transaction);
                connection.Execute("DELETE FROM " + GroupMessageTable.TableName + " WHERE ToGroupId = @id",
                    new { id = groupId }, transaction);
                
                connection.Execute("DELETE FROM " + GroupTable.TableName + " WHERE Id = @id",
                    new { id = groupId}, transaction);
                    
                transaction.Commit();
            }
        }

        public IList<Group> GetAdministratedGroups(int usedId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var sql = string.Format("SELECT GroupId FROM {0} WHERE UserId = @userId AND IsAdmin = true",
                    GroupUserTable.TableName);
                var adminGroupIds = connection.Query<int>(sql).ToList();
                return adminGroupIds.Select(GetGroup).ToList();
            }
        }

        public void SaveGroupMessage(UserMessage message, int fromUserId, int toGroupId)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                var groupMessageModel = GroupMessageMapper.Map(message, fromUserId, toGroupId);
                connection.Insert(groupMessageModel);
            }
        }

        public IList<UserMessage> GetGroupMessages(int groupId, int maxMessages=20)
        {
            using (var connection = _santaDatabase.GetOpenConnection())
            {
                   var sql = string.Format("SELECT TOP {0} u.UserName, m.CreateDate, m.[Message]  FROM {1} m JOIN {2} u ON u.Id = m.FromUserId",
                    maxMessages,
                    GroupMessageTable.TableName,
                    UserTable.TableName);
                var messages = connection.Query<UserMessage>(sql).ToList();
                return messages;
            }

        }

        private bool GroupExists(int groupId, IDbConnection connection, IDbTransaction transaction)
        {
            return connection.Query<int>(string.Format("SELECT COUNT(*) FROM {0} WHERE id = @id", GroupTable.TableName),
                    new { id = groupId}, transaction)
                    .FirstOrDefault() > 0;
        }
    }
}