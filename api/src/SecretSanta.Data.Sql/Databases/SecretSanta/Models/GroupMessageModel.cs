using System;
using Dapper.Contrib.Extensions;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    [Table(GroupMessageTable.TableName)]
    public class GroupMessageModel
    {
        public int Id { get; set; }
        public int ToGroupId { get; set; }
        public int FromUserId { get; set; }

        public DateTime CreateDate { get; set; }
        public string Message{ get; set; }

    }

}