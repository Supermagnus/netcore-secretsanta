using System;
using Dapper.Contrib.Extensions;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    [Table(GroupTable.TableName)]
    public class GroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Started { get; set; }
        public string BlacklistedPairs { get; set; }
        public string GeneratedPairs { get; set; }
        
    }


  
}