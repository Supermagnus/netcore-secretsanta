using System;
using Dapper.Contrib.Extensions;
using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    [Table(UserTable.TableName)]
    public class UserModel
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName{ get; set; }

        public string Email{ get; set; }

        public DateTime? LastLoginDate { get; set; }
        public UserRole Role { get; set; }
        public string Password { get; set; }
    }

}