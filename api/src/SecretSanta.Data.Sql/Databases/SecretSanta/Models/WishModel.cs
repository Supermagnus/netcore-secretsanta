using Dapper.Contrib.Extensions;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Models
{
    [Table(WishTable.TableName)]
    public class WishModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }

    }
}