using SecretSanta.Data.Sql.Databases.Common;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables
{
    internal class UserTable : TableBase, IDatabaseObject
    {

        public const  string TableName ="Users"; 
        

        protected override string GetConstraintsSql()
        {
            return "";
        }

        protected override string GetCreateTableSql()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, " +
                                 "UserName nvarchar(256) NOT NULL UNIQUE, " +
                                 "FirstName nvarchar(256) , " +
                                 "LastName nvarchar(256) , " +
                                 "Email nvarchar(256), " +
                                 "LastLoginDate date," +
                                 "Password nvarchar(256)," +
                                 "Role nvarchar(32)" +
                                 ")"
                                 , TableName);

        }

        protected override string GetCreateTableSqLite()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id INTEGER PRIMARY KEY, " +
                                 "UserName TEXT NOT NULL UNIQUE, " +
                                 "FirstName TEXT , " +
                                 "LastName TEXT , " +
                                 "Email TEXT, " +
                                 "LastLoginDate date," +
                                 "Password TEXT," +
                                 "Role nvarchar(32)" +
                                 ")"
                , TableName);
        }


        protected override string GetTableName()
        {
            return TableName;
        }
    }
}