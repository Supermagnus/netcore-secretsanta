using SecretSanta.Data.Sql.Databases.Common;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables
{
    internal class GroupTable : TableBase, IDatabaseObject
    {

        public const string TableName = "Groups";
        

        protected override string GetConstraintsSql()
        {
            return "";
        }

        protected override string GetCreateTableSql()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, " +
                                 "Name nvarchar(256) NOT NULL, " +
                                 "Started date , " +
                                 "BlacklistedPairs nvarchar(MAX) , " +
                                 "GeneratedPairs nvarchar(MAX))"
                                 , TableName);

        }

        protected override string GetCreateTableSqLite()
        {

            return string.Format("CREATE TABLE {0} (" +
                                 "Id INTEGER PRIMARY KEY, " +
                                 "Name TEXT NOT NULL, " +
                                 "Started date , " +
                                 "BlacklistedPairs TEXT , " +
                                 "GeneratedPairs TEXT)"
                , TableName);
        }


        protected override string GetTableName()
        {
            return TableName;
        }
    }
}