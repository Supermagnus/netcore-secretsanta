using SecretSanta.Data.Sql.Databases.Common;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables
{
    internal class UserMessageTable : TableBase, IDatabaseObject
    {

        public const string TableName="UserMessages"; 
        

        protected override string GetConstraintsSql()
        {
            return
            CreateForeignKeySql(
                UserTable.TableName, "Id",
                this.GetTableName(), "ToUserId") 
                +" "+
                CreateForeignKeySql(
                UserTable.TableName, "Id",
                this.GetTableName(), "FromUserId") ;
        }

        protected override string GetCreateTableSql()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, " +
                                 "ToUserId int NOT NULL, " +
                                 "FromUserId int NOT NULL, " +
                                 "Message nvarchar(1024), " +
                                 "CreateDate datetime)" 
                                 , TableName);

        }

        protected override string GetCreateTableSqLite()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id INTEGER PRIMARY KEY, " +
                                 "ToUserId int NOT NULL, " +
                                 "FromUserId int NOT NULL, " +
                                 "Message TEXT, " +
                                 "CreateDate datetime)"
                , TableName);
        }


        protected override string GetTableName()
        {
            return TableName;
        }
    }
}