using System.Runtime.InteropServices;
using SecretSanta.Data.Sql.Databases.Common;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables
{
    internal class GroupMessageTable : TableBase, IDatabaseObject
    {

        public const string TableName ="GroupMessages";
        protected override string GetConstraintsSql()
        {
            return 
                CreateForeignKeySql(
                    GroupTable.TableName, "Id",
                    UserTable.TableName, "FromUserId")
                +"\r\n"+
                CreateForeignKeySql(
                   UserTable.TableName, "Id",
                   this.GetTableName(), "FromUserId");

        }

        protected override string GetCreateTableSql()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id int IDENTITY(1,1) PRIMARY KEY NOT NULL, " +
                                 "ToGroupId int NOT NULL, " +
                                 "FromUserId int NOT NULL, " +
                                 "Message nvarchar(1024), " +
                                 "CreateDate datetime)"
                , TableName);
        }

        protected override string GetCreateTableSqLite()
        {
            return string.Format("CREATE TABLE {0} (" +
                                 "Id INTEGER PRIMARY KEY, " +
                                 "ToGroupId int NOT NULL, " +
                                 "FromUserId int NOT NULL, " +
                                 "Message TEXT, " +
                                 "CreateDate datetime)"
                , TableName);
        }

        protected override string GetTableName()
        {
            return TableName;
        }
    }
}

