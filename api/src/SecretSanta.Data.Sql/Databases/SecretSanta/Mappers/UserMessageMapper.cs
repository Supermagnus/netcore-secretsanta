using System;
using System.Runtime.InteropServices;
using SecretSanta.Core.Domain;
using SecretSanta.Data.Sql.Databases.Common;
using SecretSanta.Data.Sql.Databases.SecretSanta.Models;

namespace SecretSanta.Data.Sql.Databases.SecretSanta.Mappers
{
    public static class UserMessageMapper 
    {
       
        public static UserMessageModel Map(UserMessage sourceObject, int fromUserId, int toUserId)
        {
            return new UserMessageModel()
            {
                CreateDate = sourceObject.CreateDate,
                FromUserId = fromUserId,
                ToUserId = toUserId,
                Message = sourceObject.Message
            };
        }

        //public static UserMessage MapBack(UserMessageModel sourceObject)
        //{
        //    return new UserMessage()
        //    {
        //        CreateDate = sourceObject.CreateDate,
        //        Message = sourceObject.Message,
        //        UserName = sourceObject.FromUserName
                
        //    };
        //}
    }
}