using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using Dapper;

namespace SecretSanta.Data.Sql.Databases.Common
{
    public abstract class TableBase
    {
        protected abstract string GetConstraintsSql();
        protected abstract string GetCreateTableSql();
        protected abstract string GetCreateTableSqLite();
        protected abstract string GetTableName();

        /// <summary>
        ///     Gets the table name without dbo namespace and brackets
        ///     [dbo].[jobs] -> jobs
        /// </summary>
        /// <returns></returns>
        protected virtual string GetPlainTableName()
        {
            return GetTableName().Replace("[dbo].", "").Replace("[", "").Replace("]", "");
        }

        public bool Exists(DbConnection connection)
        {
            OpenConnectionIfClosed(connection);
            if (connection is SqlConnection)
            {
                var sql =
                    $"(SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{GetPlainTableName()}')";
                var count = connection.Query<int>(sql).FirstOrDefault();
                return count > 0;
            }
            if (connection is SQLiteConnection)
            {
                var sql =
                    $"SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='{GetPlainTableName()}'";
                var count = connection.Query<int>(sql).FirstOrDefault();
                return count > 0;
            }
            throw new NotSupportedException("Connection of type " + connection.GetType().Name + " is not supported.");
        }

        public void Create(DbConnection connection)
        {
            OpenConnectionIfClosed(connection);
            if (connection.State != ConnectionState.Open)
                connection.Open();
            if (connection is SqlConnection)
            {
                connection.Execute(GetCreateTableSql());
            }
            if (connection is SQLiteConnection)
            {
                connection.Execute(GetCreateTableSqLite());
            }
            else
            {
                throw new NotSupportedException("Connection of type " + connection.GetType().Name + " is not supported.");
            }

        }

        public void SetConstraints(DbConnection connection)
        {
            var sql = GetConstraintsSql();
            if (string.IsNullOrEmpty(sql) || !(connection is SqlConnection))
                return;

            OpenConnectionIfClosed(connection);
            if (connection.State != ConnectionState.Open)
                connection.Open();
            connection.Execute(sql);
        }

        public void Drop(DbConnection connection)
        {
            OpenConnectionIfClosed(connection);
            connection.Execute($"DROP TABLE {GetTableName()}");
        }

        private void OpenConnectionIfClosed(DbConnection conn)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
        }


        protected string BuildIndexSql(params string[] columns)
        {
            var sql = new StringBuilder();
            foreach (var column in columns)
            {
                var cleanTableName = GetTableName().Replace("[", "").Replace("]", "").Replace(".", "_");
                sql.AppendLine($"CREATE INDEX IX_{cleanTableName}_{column} " +
                               $"ON {GetTableName()} ({column}) " +
                               $"");
            }
            return sql.ToString();
        }

        protected string CreateForeignKeySql(
            string primaryKeyTable, string primaryKeyTableColumn,
            string foreignKeyTable, string foreignKeyTableColumn)
        {
            return
                string.Format("ALTER TABLE [dbo].[{2}]  WITH CHECK ADD  CONSTRAINT [FK_{2}_{0}_{3}] FOREIGN KEY([{3}]) REFERENCES [dbo].[{0}] ([{1}]) " +
                              "ALTER TABLE [dbo].[{2}] CHECK CONSTRAINT [FK_{2}_{0}_{3}]",
                    primaryKeyTable,
                    primaryKeyTableColumn,
                    foreignKeyTable,
                    foreignKeyTableColumn
                );
        }
    }
}