using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using Dapper;
using Findwise.Mythology.Common.Core.Settings;
using Serilog;

namespace SecretSanta.Data.Sql.Databases.Common
{
    public abstract class DatabaseBase
    {
        private readonly string _connectionString;
        private readonly string _provider;
        private bool _isSetup;
        protected IList<IDatabaseObject> DbObjects;

        protected DatabaseBase(ConnectionStringSettings connectionStringSettings)
        {
            _connectionString = connectionStringSettings.ConnectionString;
            _provider = connectionStringSettings.ProviderName;
            DbObjects = new List<IDatabaseObject>();
        }

        /// <summary>
        ///     Verifies the integrity of the database
        ///     Creates tables that does not exist
        /// </summary>
        /// <param name="conn"></param>
        public virtual void Setup(DbConnection conn)
        {
            var createdObjects = new List<IDatabaseObject>();

            foreach (var dbOject in DbObjects.Where(dbOject => !dbOject.Exists(conn)))
            {
                try
                {
                    dbOject.Create(conn);
                    createdObjects.Add(dbOject);
                }
                catch (Exception e)
                {
                    Log.Error(e, "Error when creating {name}", dbOject.GetType().Name);
                    throw;
                }
            }
            foreach (var dbOject in createdObjects)
            {
                try
                {
                    dbOject.SetConstraints(conn);
                }
                catch (Exception e)
                {
                    Log.Error(e, "Error when setting constraints on {name}", dbOject.GetType().Name);
                    throw;
                }
            }


            _isSetup = true;
        }

        /// <summary>
        ///     Verifies the integrity of the database
        ///     Creates tables that does not exist
        /// </summary>
        public void Setup()
        {
            Setup(GetOpenConnection());
        }

        /// <summary>
        ///     Will run only if Setup hasnt been executed yet
        ///     Verifies the integrity of the database
        ///     Creates tables that does not exist
        /// </summary>
        public void SetupOnce()
        {
            if (_isSetup)
                return;
            lock (_connectionString)
            {
                if (_isSetup)
                    return;
                Setup();
            }
        }

        public void Destroy()
        {
            Log.Information("Deleting database objects:");
            using (var connection = GetOpenConnection())
            {
                foreach (var databaseObject in DbObjects)
                {
                    databaseObject.Drop(connection);
                }
            }
        }

        public DbConnection GetOpenConnection()
        {

            if (string.IsNullOrEmpty(_provider))
            {
                var conn = new SqlConnection(_connectionString);
                conn.Open();
                return conn;
            }
            else if (_provider == "System.Data.SQLite")
            {
                var conn = new SQLiteConnection(_connectionString);
                conn.Open();
                return conn;
            }

            throw new Exception(
                "Only a connection string with provider of type System.Data.SQLite or nothing is supported");
        }

        protected int ExecuteSql(DbConnection connection, string sql)
        {
            return connection.Execute(sql);
        }

    }
}