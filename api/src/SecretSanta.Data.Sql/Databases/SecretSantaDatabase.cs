using System.Collections.Generic;
using SecretSanta.Data.Sql.Databases.Common;
using SecretSanta.Data.Sql.Databases.SecretSanta.Objects.Tables;
using SecretSanta.Data.Sql.Settings;

namespace SecretSanta.Data.Sql.Databases
{




    public class SecretSantaDatabase : DatabaseBase
    {
        public SecretSantaDatabase(SqlSettings settings)
            : base(settings.SecretSantaConnectionString)
        {

            DbObjects = new List<IDatabaseObject>()
                                  {
                                      new UserTable(),
                                      new UserMessageTable(),
                                      new WishTable(),
                                      new GroupTable(),
                                      new GroupUserTable(),
                                      new GroupMessageTable()
                          };


        }


    }
}