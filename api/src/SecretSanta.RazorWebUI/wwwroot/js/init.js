﻿$(document).ready(function () {

    theGame.init();


    $(document).snowfall({
        flakeCount: 15,
        maxSpeed: 5,
        minSize: 1,
        maxSize: 10,
        round: true,
        collection: '.snowfallCollector'
        
        
    });

    $("#clear").click(function () {
        $(document).snowfall('clear'); // How you clear
    });
   
});