using StructureMap;

namespace SecretSanta.RazorWebUI.Infrastructure.CastleIntegration.Installers
{
    public class ModelMapperRegistry : Registry
    {
        public ModelMapperRegistry()
        {
            Scan(config =>
            {
                config.AssemblyContainingType<IModelMapper>();
                config.AddAllTypesOf<IModelMapper>();
                config.SingleImplementationsOfInterface();
            });
        }
    }
}