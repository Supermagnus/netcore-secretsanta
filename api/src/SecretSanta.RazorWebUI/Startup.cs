﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.Core.Infrastructure;
using SecretSanta.Data.Sql.Infrastructure;
using SecretSanta.RazorWebUI.Games;
using SecretSanta.RazorWebUI.Infrastructure;
using SecretSanta.RazorWebUI.Infrastructure.CastleIntegration.Installers;
using Serilog;
using Serilog.Events;
using StructureMap;

namespace SecretSanta.RazorWebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;


            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.LiterateConsole()
                .WriteTo.RollingFile(".data/logs/log-{Date}.txt")
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Information()
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(option =>
                {
                    option.AccessDeniedPath = new PathString("/Login/LoginDenied");
                    option.LoginPath = new PathString("/Login");
                    option.ExpireTimeSpan = new TimeSpan(5,0,0,0);
                    
                });
            services.AddMvc();
           

            var serviceProvider = BootstrapStructuremap(services);
            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "Login", 
                    "Login",
                    defaults: new {controller = "Login", action = ""});
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Login}/{action}");
            });
            loggerFactory.AddSerilog(dispose: true);
            app.UseMiddleware<SerilogMiddleware>();
        }

        private IServiceProvider BootstrapStructuremap(IServiceCollection services)
        {


            var container = new Container(config =>
            {
                config.IncludeRegistry<SqlRegistry>();
                config.IncludeRegistry<CoreRegistry>();
                config.IncludeRegistry<ModelMapperRegistry>();
                config.Populate(services);
            });



            container.AssertConfigurationIsValid();


            var userService = container.GetInstance<IUserService>();
            var groupService = container.GetInstance<IGroupService>();
            new VikgatanGame(userService, groupService).Init(true);
            //new TestGame(userService,groupService).Init(true);

            return container.GetInstance<IServiceProvider>();

        }

    }
}
