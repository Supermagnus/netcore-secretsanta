﻿using System.Collections.Generic;
using System.Linq;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;

namespace SecretSanta.RazorWebUI.Games
{
    public class TestGame : PremadeGame
    {
        public TestGame(IUserService userService, IGroupService groupService) : base(userService, groupService)
        {
        }


        protected override IList<User> CreateUsers()
        {
            return new List<User>()
            {
                new User()
                {
                    Id = 11,
                    UserName = "Magnus",
                    Email = "supermagnus@gmail.com",
                    FirstName = "Magnus",
                    LastName = "Månsson",
                    Password = "mooning",
                    Role = UserRole.Admin
                },
                new User()
                {
                    Id = 22,
                    UserName = "Magnus 2",
                    Email = "supermagnus@gmail.com",
                    FirstName = "Magnus",
                    LastName = "Månsson",
                    Password = "mooning",
                    Role = UserRole.Admin
                }
            };
        }

        protected override IList<Group> CreateGroups()
        {
            return new List<Group>()
            {
                new Group()
                {
                    Name = "test 2017",
                    UserIds = Users.Select(x => x.Id).ToList(),
                    BlacklistedPairs = new List<UserPair>()
                }
            };
        }
    }
}