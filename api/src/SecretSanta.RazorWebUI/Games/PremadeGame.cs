﻿using System.Collections.Generic;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;

namespace SecretSanta.RazorWebUI.Games
{
    public abstract class PremadeGame
    {
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;

        protected IList<User> Users;
        protected IList<Group> Groups;
        protected PremadeGame(IUserService userService, IGroupService groupService)
        {
            _userService = userService;
            _groupService = groupService;
            
            Users = CreateUsers();
        }

        
        protected abstract IList<User> CreateUsers();
        protected abstract IList<Group> CreateGroups();

        public void Init(bool overwriteExisting)
        {
            
            foreach (var user in Users)
            {
                var currentUser = _userService.GetUser(user.UserName);
                if (!overwriteExisting && currentUser != null)
                    continue;
                if (currentUser != null)
                    user.Id = currentUser.Id;
                _userService.SaveUser(user);
            }
            Groups =CreateGroups();

            foreach (var group in Groups)
            {
                if(_groupService.GetGroup(group.Name) == null)
                    _groupService.SaveGroup(group);
            }
        }
    }
}