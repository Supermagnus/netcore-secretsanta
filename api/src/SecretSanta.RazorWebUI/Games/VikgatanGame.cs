﻿using System.Collections.Generic;
using System.Linq;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;

namespace SecretSanta.RazorWebUI.Games
{
    public class VikgatanGame : PremadeGame
    {
        public VikgatanGame(IUserService userService, IGroupService groupService) : base(userService, groupService)
        {
        }


        protected override IList<User> CreateUsers()
        {
            return new List<User>()
            {
                new User()
                {
                    Id = 1,
                    UserName = "Magnus",
                    Email = "supermagnus@gmail.com",
                    FirstName = "Magnus",
                    LastName = "Månsson",
                    Password = "mooning",
                    Role = UserRole.Admin
                },
                new User()
                {
                    Id = 2,
                    UserName = "Sanna",
                    Email = "sanna.eveby@gmail.com",
                    FirstName = "Sanna",
                    LastName = "Eveby",
                    Password = "sannaärbäst"
                },
                new User()
                {
                    Id = 3,
                    UserName = "Bodil",
                    Email = "bodilman@hotmail.com",
                    FirstName = "Bodil",
                    LastName = "Månsson",
                    Password = "boddanärbäst"
                },
                new User()
                {
                    Id = 5,
                    UserName = "Lars",
                    Email = "mansson1952@hotmail.com",
                    FirstName = "Lars",
                    LastName = "Månsson",
                    Password = "larsärbäst"
                },
                new User()
                {
                    Id = 4,
                    UserName = "Johanna",
                    Email = "mansson.johanna@gmail.com",
                    FirstName = "Johanna",
                    LastName = "Månsson",
                    Password = "magnusärbäst"
                },
                new User()
                {
                    Id = 6,
                    UserName = "Pontus",
                    Email = "pontus.klaren@hotmail.com",
                    FirstName = "Pontus",
                    LastName = "Klarén",
                    Password = "pantusärbäst"
                }
            };
        }

        protected override IList<Group> CreateGroups()
        {
            return new List<Group>()
            {
                new Group()
                {
                    Name = "Vikgatan 2017",
                    UserIds = Users.Select(x => x.Id).ToList()
                }
            };
        }
    }
}