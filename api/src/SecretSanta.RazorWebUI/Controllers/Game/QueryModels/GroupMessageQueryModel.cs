﻿namespace SecretSanta.WebUI.Controllers.Game.QueryModels
{
    public class GroupMessageQueryModel : QueryModelBase
    {
        public int GroupId{ get; set; }
        public string Message { get; set; }

    }
}