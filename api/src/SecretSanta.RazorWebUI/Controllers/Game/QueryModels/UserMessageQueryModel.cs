﻿namespace SecretSanta.WebUI.Controllers.Game.QueryModels
{
    public class UserMessageQueryModel : QueryModelBase
    {

        public int ReceiverUserId { get; set; }
        public string Message{ get; set; }
        
    }
}