﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.RazorWebUI.Attributes;
using SecretSanta.WebUI.Controllers.Game.Domain;
using SecretSanta.WebUI.Controllers.Game.ModelMappers;
using SecretSanta.WebUI.Controllers.Game.QueryModels;
using SecretSanta.WebUI.Controllers.Game.ViewModels;

namespace SecretSanta.RazorWebUI.Controllers.Game
{
    [Authorize]
    public class GameController : Controller
    {
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;
        private readonly IGamePageModelMapper _gamePageMapper;


        public GameController(IUserService userService, IGroupService groupService, IGamePageModelMapper gamePageMapper){
            _userService = userService;
            _groupService = groupService;
            _gamePageMapper = gamePageMapper;
        }


        [Route("")]
        [Route("Game/GamePage")]
        public ActionResult GamePage(string currentGroup = "",string currentTab ="")
        {
            var loggedInUser = GetLoggedInUser();

            var availableGroups = _groupService.GetGroupsForUser(loggedInUser.UserName);
            if (!availableGroups.Any() || !availableGroups.Any(x => x.IsStarted()))
                return View("NoGroup", loggedInUser);
            
            var selectedGroup = new CurrentGroup()
            {
                Group = availableGroups.FirstOrDefault(x => x.Name == currentGroup) ?? availableGroups.FirstOrDefault()
            };
            selectedGroup.GroupMessages = _groupService.GetGroupMessages(selectedGroup.Group.Id, 150);
            var linkedUser = GetLinkedUsers(loggedInUser, selectedGroup.Group);

            var model = new GamePageViewModel
            {
                CurrentGroup = selectedGroup.Group.Name,
                CurrentTab = currentTab,
                GroupChatPresenterViewModel =
                    _gamePageMapper.MapGroupChatPresenter(
                    loggedInUser, selectedGroup.Group, selectedGroup.GroupMessages, 
                    selectedGroup.Group.UserIds.Select(userId => _userService.GetUser(userId)).ToList()),
                LoggedInUser = loggedInUser,
                MenuViewModel = new MenuViewModel()
                {
                    GameGroupMenuViewModel = _gamePageMapper.MapGameGroupMenu(availableGroups, selectedGroup.Group)
                },
                ReceivingUserPresenterViewModel =
                    _gamePageMapper.MapReceivingUserViewModel(linkedUser, selectedGroup.Group,
                        _userService.GetWishes(linkedUser.ReceiverUser.Id)),
                UserWishlistPresenterViewModel =
                    _gamePageMapper.MapUserWishlist(linkedUser,_userService.GetWishes(loggedInUser.Id))
            };
            return View("GamePage",model);
        }



        public string StartGameForUser()
        {
            var user = GetLoggedInUser();
            user.LastLoginDate = DateTime.Now;
            _userService.SaveUser(user);
            return "Kör på!";
        }

        

        public JsonResult SaveOrUpdateWish(WishQueryModel wishQueryModel)
        {
            var loggedInUser = GetLoggedInUser();
            var wish = new Wish()
            {
                Description = wishQueryModel.Wish,
                Id = wishQueryModel.WishId
            };

            _userService.SaveWish(wish, loggedInUser.Id);
            return Json("ok");
        }

        public ActionResult AddWish(WishQueryModel wishQueryModel)
        {
            var loggedInUser = GetLoggedInUser();
            var wish = new Wish()
            {
                Description = wishQueryModel.Wish,
                Id = wishQueryModel.WishId
            };
            _userService.SaveWish(wish, loggedInUser.Id);
            return RedirectToAction("GamePage", new { currentGroup = wishQueryModel.CurrentGroup, currentTab = wishQueryModel.CurrentTab });
        }

        public ActionResult DeleteWish(WishQueryModel wishQueryModel)
        {
            var loggedInUser = GetLoggedInUser();

            _userService.DeleteWish(wishQueryModel.WishId,loggedInUser.Id);
            return RedirectToAction("GamePage", new { currentGroup = wishQueryModel.CurrentGroup, currentTab = wishQueryModel.CurrentTab });

        }

       
        public ActionResult SendUserMessage(UserMessageQueryModel messageQueryModel)
        {
            var loggedInUser = GetLoggedInUser();

            _userService.SaveUserMessage(
                new UserMessage()
                {
                    CreateDate = DateTime.Now,
                    Message = messageQueryModel.Message,
                    UserName = loggedInUser.UserName
                },
                loggedInUser.Id,
                messageQueryModel.ReceiverUserId);
            
            return RedirectToAction("GamePage",  new {messageQueryModel.CurrentGroup, messageQueryModel.CurrentTab});

        }

        public ActionResult SendGroupMessage(GroupMessageQueryModel groupMessage)
        {
            var group = _groupService.GetGroup(groupMessage.GroupId);
            var loggedInUser = GetLoggedInUser();

            if (group == null || loggedInUser == null)
                return RedirectToAction("GamePage");
            _groupService.SaveGroupMessage(
                new UserMessage()
                {
                    CreateDate = DateTime.Now,
                    UserName = loggedInUser.UserName,
                    Message = groupMessage.Message
                },
                loggedInUser.Id,
                group.Id);
            
            return RedirectToAction("GamePage", new { groupMessage.CurrentGroup, groupMessage.CurrentTab });

        }





        private LinkedUsers GetLinkedUsers(User loggedInUser, Group group)
        {
            var secretSantaUser = _userService.GetUser(group.GetPairWhereUserIsReceiver(loggedInUser.Id).SecretSantaId);
            var receiverUser = _userService.GetUser((group.GetPairWhereUserIsSecretSanta(loggedInUser.Id).ReceiverId));
            var secretSantaChat =
                _userService.GetUserMessages(MessageFilter.CreateUserConversationFilter(loggedInUser.Id,
                    secretSantaUser.Id));
            var receiverChat =
                _userService.GetUserMessages(MessageFilter.CreateUserConversationFilter(loggedInUser.Id,
                    receiverUser.Id));
            return new LinkedUsers()
            {
                LoggedInUser = loggedInUser,
                ReceiverUser = receiverUser,
                SecretSantaUser = secretSantaUser,
                SecretSantaConversation = GetMessageConversation(secretSantaChat, secretSantaUser.UserName),
                ReceivingUserConversation = GetMessageConversation(receiverChat)
            };
        }
        private IList<Message> GetMessageConversation( IList<UserMessage> messages,string anonymizeUsername ="" )
        {
            return messages.Select(
                msg => new Message()
                {
                    Sender = string.IsNullOrEmpty(anonymizeUsername) ? msg.UserName
                        : msg.UserName == anonymizeUsername ? "??" : msg.UserName,
                    Text = msg.Message,
                    CreateDate = msg.CreateDate
                }).ToList();
        }

        private User GetLoggedInUser()
        {
            var loggedInUser = _userService.GetUser(HttpContext.User.Identity.Name);
            if (loggedInUser == null)
                throw new AuthenticationException("No user named " + HttpContext.User.Identity.Name + " found");
            
            if (!loggedInUser.LastLoginDate.HasValue || (DateTime.Now - loggedInUser.LastLoginDate.Value).TotalMinutes > 10)
            {
                loggedInUser.LastLoginDate = DateTime.Now;
                _userService.SaveUser(loggedInUser);
            }
            return loggedInUser;
        }
    }
}
