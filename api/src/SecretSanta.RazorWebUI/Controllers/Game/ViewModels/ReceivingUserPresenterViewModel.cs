﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SecretSanta.Core.Domain;
using SecretSanta.WebUI.Controllers.Game.Domain;

namespace SecretSanta.WebUI.Controllers.Game.ViewModels
{
    public class ReceivingUserPresenterViewModel
    {

        public string GroupName { get; set; }
        public User ReceivingUser { get; set; }
        public IList<Wish> ReceivingUserWishlist { get; set; } 
        public AnonUserChatViewModel AnonUserChatViewModel { get; set; }
     
        
    }
}