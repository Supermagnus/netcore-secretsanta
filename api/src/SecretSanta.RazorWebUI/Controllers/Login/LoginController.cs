﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using SecretSanta.Core.ApplicationServices.Interfaces;
using SecretSanta.Core.Domain;
using SecretSanta.RazorWebUI.Controllers.Login.ViewModels;
using SecretSanta.RazorWebUI.Helpers;
using Serilog;

namespace SecretSanta.RazorWebUI.Controllers
{
    [Route("[controller]")]
    public class LoginController : Controller
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("")]
        public IActionResult Index()
        {
            return View("LoginPage");
        }

        [HttpPost]
        public async Task<IActionResult> Login(string userName,string password)
        {
            try
            {
                var user = GetUser(userName, password);
            
            // create claims
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, userName),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, user.Role.ToString())
            };
            // create identity
            ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

            // create principal
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            // sign-in
            await HttpContext.SignInAsync(
                scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                principal: principal);

            return RedirectToAction("GamePage", "Game");
            }
            catch (AuthenticationException ae)
            {
                return View("LoginPage", new LoginViewModel()
                {
                    DeniedMessage = "Ingen användare med det lösenordet :("
                });
            }
        }
        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login");
        }



        private User GetUser(string userName, string password)
        {
            var existingUser = _userService.GetUser(userName);
            if (existingUser == null || existingUser.Password != password) { 
                Log.Warning($"Login failed for username {userName} and password {password}");
                throw new AuthenticationException();
            }
            Log.Information($"Login success for {userName}");
            return existingUser;
        }
        
    }
}
