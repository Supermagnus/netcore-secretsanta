using SecretSanta.Data.FileSystem.Settings;
using StructureMap;

namespace SecretSanta.Data.FileSystem.Infrastrucutre
{
    public class FileSystemRegistry : Registry
    {

        public FileSystemRegistry()
        {
            ForSingletonOf<FileSystemSettings>().Use<FileSystemSettings>();
        }}
}